import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
html {
    -webkit-box-sizing: border-box;
    box-sizing: border-box
}

*,
*::after,
*::before {
    -webkit-box-sizing: inherit;
    box-sizing: inherit;
    font-family: "Open Sans";
}

ul,
ol {
    padding: 0
}

body,
h1,
h2,
h3,
h4,
h5,
h6,
p,
ul,
ol,
li,
figure,
figcaption,
blockquote,
dl,
dd {
    margin: 0
}

ul[class] {
    list-style: none
}

img {
    max-width: 100%;
    display: block
}

input,
button,
textarea,
select {
    font: inherit
}

a {
    text-decoration: none;
    color: inherit
}

button {
    border: none;
    cursor: pointer
}

input {
    border: none;
    outline: none
}

html,
body {
    height: 100%
}

body {
    font-family: 'Montserrat', sans-serif;
    font-weight: 600;
    font-size: 18px;
    line-height: 30px;
}
`;