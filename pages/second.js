import Head from 'next/head'
import Image from 'next/image'
import cookies from 'next-cookies'
import { ProductSlider } from '../src/components/ui/productSlider'
import { Layout } from '../src/components/common/layout'
import Category from '../src/components/ui/category'
import CategorySlider from '../src/components/ui/categorySlider'
import { BannerImage } from '../src/components/ui/banner/styled'
import Banner from '../src/components/ui/banner'
import SearchBar from 'src/components/ui/searchBar'
import { Breadcrumbs } from 'src/components/ui/breadcrumbs'
import { Product } from 'src/components/ui/product'
import { getCategories, getRecommended } from 'api'

export default function Second({ time, recommended, categories }) {
  const times = {
    'ng': {
      title: 'Новый год близко',
      subtitle: 'Мы собрали для вас подборку полезных товаров к этому празднику',
      image: 'https://s3-alpha-sig.figma.com/img/7136/a352/211d98db9edce326beb902c6b8e9c8d6?Expires=1631491200&Signature=J-vz3vrQb8lV1-hU5rsTZux-Uo4Go3xj1XilTLBm4XcssrFvvEZBPIaYe7n5qypCEI33jgXtadd90p3Dws84L9Cw1YoxOCJumOXSlkPYt1fRI2Xyha-QrY9LT3XdbbrEs8Dw3vuD1fSbOTPNk-nKez8cviFw6vesQ0t7fR-sT3yYKxRcLeyTMAb7sGSLrGsZty962u1WrH-4rVJn4wGgP-4wh7kx12u3VQLZHuEyOcbK3btfCFqqWiJWd4XaYEBLL0RLA0ndZE4~Sf9fpLSHe68Jd5aNUIqgqETOmopGY36jEWhZ1ywDTOD4V0HnVXdUxMJIzLzepeGw0KxKA5Iefg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
      buttonText: 'Посмотреть товары из подборки'
    },
    '1': {
      title: 'С первым сентября!',
      subtitle: 'Мы собрали для вас подборку полезных товаров к этому празднику',
      image: 'https://s3-alpha-sig.figma.com/img/b2c9/6e26/3e0679dc6db76562d48df2b42afbbfad?Expires=1631491200&Signature=PWuLLwrHqy0b2aJRxcTH9J0BU4Ogfq5Osq6z51YWsNqitbdc~r-m6ZYfhH20oYrA8M7wHraDvc2n9guoxEokihd5GU9~hcrex5q-gMTuB6pS4phLzWB47au7g7YcooTFdIFAGOy30gI9nnrHOfkUN5niiNuDI41zISwtwUsT2ceD7saD8oTSVxe6jTuGCPc5o65JHaekJlwUAyv95vQIc4A8uWbyEK4u~0RaiGMEGb872ziKrCHJkRrZt5v9t4yGCUObzGYwoUVI-qsXNnAfkPM3Lc6nDsOYcxCUUdZUDUCqV9bHJaZ-t4rt8BVKrHLQuRsfVkuz-FP39g10LZV9hg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
      buttonText: 'Посмотреть товары из подборки'
    },
  }
  return (
    <>
      <SearchBar />
      <Banner
        { ...times[time]}
      />
      <Layout>
        <CategorySlider
          categories={categories}
        />
        <ProductSlider
          title={'Вам может быть интересно'}
          cards={recommended.length <= 3 ? recommended.concat(recommended) : recommended}
        />
      </Layout>
    </>
  )
}


export async function getServerSideProps(ctx) {
  const time = cookies(ctx).time;

  let recommended;
  let categories;
  let userId = cookies(ctx).userId;
  if (userId === undefined) {
      recommended = await getRecommended(1140358);
      categories = await getCategories(1140358);
  } else {
      recommended = await getRecommended(userId);
      categories = await getCategories(userId);
  }

  return {
    props: {
      time,
      recommended,
      categories
    }
  }
}
