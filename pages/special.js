import cookies from 'next-cookies'
import { ProductSlider } from '../src/components/ui/productSlider'
import { Layout } from '../src/components/common/layout'
import Banner from '../src/components/ui/banner'
import SearchBar from 'src/components/ui/searchBar'
import { getCategories, getNewYear, getRecommended } from 'api'
import { multiplyItems } from 'utils'

export default function Second({ time, recommended }) {
  const times = {
    'ng': {
      title: 'Новый год близко',
      subtitle: 'Мы собрали для вас подборку полезных товаров к этому празднику',
      image: 'https://s3-alpha-sig.figma.com/img/7136/a352/211d98db9edce326beb902c6b8e9c8d6?Expires=1631491200&Signature=J-vz3vrQb8lV1-hU5rsTZux-Uo4Go3xj1XilTLBm4XcssrFvvEZBPIaYe7n5qypCEI33jgXtadd90p3Dws84L9Cw1YoxOCJumOXSlkPYt1fRI2Xyha-QrY9LT3XdbbrEs8Dw3vuD1fSbOTPNk-nKez8cviFw6vesQ0t7fR-sT3yYKxRcLeyTMAb7sGSLrGsZty962u1WrH-4rVJn4wGgP-4wh7kx12u3VQLZHuEyOcbK3btfCFqqWiJWd4XaYEBLL0RLA0ndZE4~Sf9fpLSHe68Jd5aNUIqgqETOmopGY36jEWhZ1ywDTOD4V0HnVXdUxMJIzLzepeGw0KxKA5Iefg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
      buttonText: 'Посмотреть товары из подборки'
    },
    '1': {
      title: 'С первым сентября!',
      subtitle: 'Мы собрали для вас подборку полезных товаров к этому празднику',
      image: 'https://s3-alpha-sig.figma.com/img/b2c9/6e26/3e0679dc6db76562d48df2b42afbbfad?Expires=1631491200&Signature=PWuLLwrHqy0b2aJRxcTH9J0BU4Ogfq5Osq6z51YWsNqitbdc~r-m6ZYfhH20oYrA8M7wHraDvc2n9guoxEokihd5GU9~hcrex5q-gMTuB6pS4phLzWB47au7g7YcooTFdIFAGOy30gI9nnrHOfkUN5niiNuDI41zISwtwUsT2ceD7saD8oTSVxe6jTuGCPc5o65JHaekJlwUAyv95vQIc4A8uWbyEK4u~0RaiGMEGb872ziKrCHJkRrZt5v9t4yGCUObzGYwoUVI-qsXNnAfkPM3Lc6nDsOYcxCUUdZUDUCqV9bHJaZ-t4rt8BVKrHLQuRsfVkuz-FP39g10LZV9hg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
      buttonText: 'Посмотреть товары из подборки'
    }
  }
  return (
    <>
      <SearchBar />
      <Banner
        disableButton
        { ...times[time]}
      />
      <Layout>
        <ProductSlider
          title={'Специально в этот день!'}
          cards={multiplyItems(recommended)}
        />
      </Layout>
    </>
  )
}


export async function getServerSideProps(ctx) {
  const time = cookies(ctx).time;

  let recommended;
  let type = cookies(ctx).time;

  if (type === 'ng') {
    recommended = await getNewYear();
  } else if (type === '1') {
    recommended = await getNewYear();
  }

  return {
    props: {
      time,
      recommended,
    }
  }
}
