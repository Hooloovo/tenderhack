import { getCategoryFamily, getRecentProducts, getRecommended, getRecommendedCategory, getRecommendedSubcategory } from "api";
import { Layout } from "src/components/common/layout";
import { Breadcrumbs } from "src/components/ui/breadcrumbs";
import { ProductSlider } from "src/components/ui/productSlider";
import ProductTable from "src/components/ui/productTable";
import SearchBar from "src/components/ui/searchBar";
import cookies from "next-cookies";
import { multiplyItems } from "utils";

export default function Category({ breadcrumbs, recent, recommended }) {
    return (
        <>
            <SearchBar />
            <Layout>
                <ProductSlider
                  title={'Вы уже заказывали'}
                  cards={recent.length > 0 ? multiplyItems(recent) : []}
                />
                <Breadcrumbs
                    path={breadcrumbs}
                />
            </Layout>
            <ProductTable cards={recommended} />
        </>
    )
}

export async function getServerSideProps(ctx) {
    const { params } = ctx;
    const breadcrumbs = await getCategoryFamily(params.id);

    let recent;
    let recommended;
    let userId = cookies(ctx).userId;
    if (userId === undefined) {
        recent = await getRecentProducts(1140358);
        if (breadcrumbs.length === 1) {
            recommended = await getRecommendedCategory(1140358, params.id)
        } else {
            recommended = await getRecommendedSubcategory(1140358, params.id)
        }
    } else {
        recent = await getRecentProducts(userId);
        if (breadcrumbs.length === 1) {
            recommended = await getRecommendedCategory(userId, params.id)
        } else {
            recommended = await getRecommendedSubcategory(userId, params.id)
        }
    }
  
    return {
        props: {
            breadcrumbs: breadcrumbs.reverse(),
            recommended,
            recent,
        }, // will be passed to the page component as props
    }
}