import { ProductSlider } from '../src/components/ui/productSlider'
import { Layout } from '../src/components/common/layout'
import { Breadcrumbs } from 'src/components/ui/breadcrumbs'
import Product from 'src/components/ui/product'
import { getProduct, getProductBreadcrumbs, getRecommended, getSimular } from 'api';
import cookies from 'next-cookies';
import { multiplyItems } from 'utils';

export default function Item({ product, sumular, breadcrumbs }) {
  return (
    <>
      <Layout>
        <Breadcrumbs path={breadcrumbs} />
        <Product { ...product} />
        <ProductSlider
          title={'С этим товаром покупают:'}
          cards={multiplyItems(sumular)}
        />
      </Layout>
    </>
  )
}

export async function getServerSideProps(ctx) {
  const { params } = ctx;
  const product = await getProduct(params.id);
  const breadcrumbs = await getProductBreadcrumbs(params.id);

  let sumular;
  let userId = cookies(ctx).userId;
  if (userId === undefined) {
    sumular = await getSimular(1140358, params.id);
  } else {
    sumular = await getSimular(userId, params.id);
  }

  return {
    props: {
      product,
      sumular,
      breadcrumbs
    }, // will be passed to the page component as props
  }
}
