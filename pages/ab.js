import Heading from "src/components/ui/ab/heading";
import Table from "src/components/ui/ab/table";
import TestingRow from "src/components/ui/testingRow";

export default function AB() {
    return (
        <>
            <Heading />
            <Table />
            <TestingRow submitable />
            <TestingRow />
        </>
    )
}