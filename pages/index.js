import { useEffect } from 'react';
import Head from 'next/head'
import Image from 'next/image'
import { ProductSlider } from '../src/components/ui/productSlider'
import { Layout } from '../src/components/common/layout'
import Category from '../src/components/ui/category'
import CategorySlider from '../src/components/ui/categorySlider'
import { BannerImage } from '../src/components/ui/banner/styled'
import Banner from '../src/components/ui/banner'
import SearchBar from 'src/components/ui/searchBar'
import { Breadcrumbs } from 'src/components/ui/breadcrumbs'
import { Product } from 'src/components/ui/product'
import { getCategories, getRecentProducts, getRecommended, getPopular } from 'api';
import cookies from 'next-cookies';
import { multiplyItems } from 'utils';

export default function Main({ recommended, categories, popular, recent }) {
  return (
    <>
      <SearchBar />
      <Layout>
        <ProductSlider
          title={'Вам может быть интересно'}
          cards={multiplyItems(recommended)}
        />
        <CategorySlider
          categories={categories}
        />
        <ProductSlider
          title={'Популярные товары'}
          cards={multiplyItems(popular)}
        />
        <ProductSlider
          title={'Вы недавно смотрели'}
          cards={recent.length > 0 ? multiplyItems(recent) : []}
        />
      </Layout>
    </>
  )
}

export async function getServerSideProps(ctx) {
  let recommended;
  let popular = await getPopular();
  let categories;
  let recent;
  let userId = cookies(ctx).userId;
  console.log(ctx.req.headers);
  if (userId === undefined) {
      recommended = await getRecommended(1140358);
      categories = await getCategories(1140358);
      recent = await getRecentProducts(1140358);
  } else {
      recommended = await getRecommended(userId);
      categories = await getCategories(userId);
      recent = await getRecentProducts(userId);
  }

  return {
    props: {
      recommended,
      categories,
      popular,
      recent
    }
  }
}
