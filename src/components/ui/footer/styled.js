import { Layout } from 'src/components/common/layout';
import { Span16 } from 'src/components/common/typography';
import styled from 'styled-components';


export const FooterContainer = styled.footer`
    width: 100%;
    padding-top: 48px;
    padding-bottom: 156px;
    background-color: rgb(245, 248, 252);
`;

export const FooterInner = styled(Layout)`
    display: flex;
    flex-direction: column;
`;

export const FooterLinks = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 32px;
`;

export const FooterLink = styled(Span16)`
    color: #264b82;
    opacity: .8;
    margin-right: 16px;
    cursor: pointer;

    &:hover {
        opacity: 1;
    }
`;

export const FooterLogos = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 32px;

    > img {
        width: 100px;
        object-fit: scale-down;
        margin-right: 16px;
    }
`;

export const FooterBottom = styled.div`
    display: flex;
    flex-direction: row-reverse;
`;

export const FooterIcon = styled.img`
    width: 38px;
    height: 38px;
    border-radius: 19px;
    margin-left: 15px;
`;
