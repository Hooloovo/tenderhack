import React from 'react';
import { FooterBottom, FooterContainer, FooterIcon, FooterInner, FooterLink, FooterLinks, FooterLogos } from './styled';


const Footer = () => {
    return (
        <FooterContainer>
            <FooterInner>
                <FooterLinks>
                    <FooterLink>О портале</FooterLink>
                    <FooterLink>Контакты</FooterLink>
                    <FooterLink>Новости</FooterLink>
                    <FooterLink>Центр поддержки</FooterLink>
                    <FooterLink>Карта сайта</FooterLink>
                </FooterLinks>
                <FooterLogos>
                    <img src="	https://zakupki.mos.ru/cms/Media/Footer/DCP_logo.png" />
                    <img src="https://zakupki.mos.ru/cms/Media/Footer/DIT_logo.png" />
                </FooterLogos>
                <FooterBottom>
                    <FooterIcon src="https://zakupki.mos.ru/static/media/facebook.8c100ab3.svg" />
                    <FooterIcon src="https://zakupki.mos.ru/static/media/twitter.9f94ba0f.svg" />
                    <FooterIcon src="https://zakupki.mos.ru/static/media/instargam.24cd6604.svg" />
                    <FooterIcon src="https://zakupki.mos.ru/static/media/vkonakte.1233b223.svg" />
                    <FooterIcon src="https://zakupki.mos.ru/static/media/youtube.ec110683.svg" />
                    <FooterIcon src="https://zakupki.mos.ru/static/media/telegram_footer.074fcc6c.svg" />
                </FooterBottom>
            </FooterInner>
        </FooterContainer>
    )
}


export default Footer;
