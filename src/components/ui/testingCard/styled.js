import styled from 'styled-components';

export const CardTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 24px;
    color: #333333;
    margin-bottom: 16px;
`

export const CardDescription = styled.p`
  font-style: normal;
    font-size: 14px;
    line-height: 20px;
    margin: 0;
    color: #333333;
`

export const Container = styled.div`
    min-width: 454px;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 164px 140px ${({ submitable }) => submitable && '57px'};
    grid-gap: 4px;
    margin-right: 40px;
`

export const CardTop = styled.div`
    grid-column-start: 1;
    grid-column-end: 3;
    padding: 32px;
    display: flex;
    flex-direction: column;
    background-color: #fff;
`

export const CardLeftRight = styled.div`
    padding: 32px;
    display: flex;
    flex-direction: column;
    background-color: #fff;
`

export const CardButton = styled.button`
    background: #CA2A26;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 18px;
    text-align: center;

    color: #FFFFFF;
    cursor: pointer;
    grid-column-start: 1;
    grid-column-end: 3;
`
