import React from 'react';
import Link from 'next/link';
import { CardContainer, CardCount, CardImage, CardName, CardPrice } from './styled';


export const ProductCard = ({ image, price, count, name, id }) => {
    return (
        <Link href={`/${id}`}>
            <CardContainer>
                <CardImage src={image} />
                <CardPrice>{price} ₽</CardPrice>
                <CardCount>{count} просмотров</CardCount>
                <CardName>{name}</CardName>
            </CardContainer>
        </Link>
    )
}
