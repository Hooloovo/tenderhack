import styled from 'styled-components';
import { Span20 } from '../../common/typography';
import { Span14, Span16 } from '../../common/typography';


export const CardContainer = styled.a`
    width: 222px;
    display: flex;
    flex-direction: column;
    cursor: pointer;
`;

export const CardImage = styled.img`
    height: 160px;
    object-fit: contain;
`;

export const CardPrice = styled(Span20)`
    margin-top: 40px;
    margin-bottom: 8px;
`;

export const CardCount = styled(Span14)`
    color: #7f8792;
    margin-bottom: 18px;
`;

export const CardName = styled(Span16)`
    max-height: 72px;
    overflow: hidden;
    font-weight: 600;
    color: #000; 
`;
