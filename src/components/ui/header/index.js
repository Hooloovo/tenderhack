import { useRouter } from 'next/router';
import { getRandomCustomer, getRecommended } from 'api';
import React, { useState, useEffect } from 'react';
import { HeaderContainer, HeaderInner, HeaderLeft, HeaderRight, HeaderUserAvatar, HeaderUserContainer, HeaderUserName, HeaderUserPopup, HeaderUserPopupButton, HeaderUserPopupTitle, HeaderDateList } from './styled';
import Link from 'next/link';


function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

const Header = () => {
    const [name, setName] = useState('')
    const [isOpen, setOpen] = useState(false);
    const [isDateOpen, setDateOpen] = useState(false);
    const router = useRouter();

    const handleCloseAll = () => {
        setOpen(false);
        setDateOpen(false);
    }
    const handleChooseTime = (time) => {
        handleCloseAll();
        document.cookie = `time=${time}; path=/`;
        router.push('/second');
    }
    const handleChangeCustomer = () => {
        handleCloseAll();
        getRandomCustomer().then((data) => {
            document.cookie = `userId=${data.id}; path=/`;
            document.cookie = `userName=${encodeURIComponent(data.name)}; path=/`;
            setName(data.name);
        });
    }

    useEffect(() => {
        let name = getCookie('userName');
        if (name === undefined) {
            getRandomCustomer().then((data) => {
                document.cookie = `userId=${data.id}; path=/`;
                document.cookie = `userName=${encodeURIComponent(data.name)}; path=/`;
                setName(data.name);
            });
        } else {
            setName(name);
        }
    }, []);
    return (
        <HeaderContainer>
            <HeaderInner>
                <Link href="/">
                    <HeaderLeft>
                        <img src="https://zakupki.mos.ru/static/media/pp_logo.80b7ad86.svg" />
                    </HeaderLeft>
                </Link>
                <HeaderRight>
                    <img src="https://zakupki.mos.ru/static/media/qa.8c4ac933.svg" />
                    <img src="https://zakupki.mos.ru/static/media/idea.42680dbc.svg" />
                    <img src="https://zakupki.mos.ru/static/media/direction.edd2fb5e.svg" />
                    <img src="https://zakupki.mos.ru/static/media/bell.8f151723.svg" />

                    <HeaderUserContainer>
                        <HeaderUserName onClick={() => setOpen(!isOpen)}>{name.search(/('|"|«|»)/gm) !== -1 ? name.match(/("|«|').+("|»|')/gm)[0] : name}</HeaderUserName>
                        <HeaderUserAvatar onClick={() => setOpen(!isOpen)} src="https://zakupki.mos.ru/static/media/user.dbcbc910.svg" />

                        <HeaderUserPopup visible={isOpen}>
                            <HeaderUserPopupTitle>Личный кабинет</HeaderUserPopupTitle>
                            <HeaderUserPopupButton onClick={() => handleChangeCustomer()}>Сменить пользователя</HeaderUserPopupButton>
                            <HeaderUserPopupButton onClick={() => setDateOpen(!isDateOpen)}>Дата</HeaderUserPopupButton>
                            <HeaderDateList visible={isDateOpen}>
                                <li onClick={() => handleChooseTime('ng')}>Новый год</li>
                                <li onClick={() => handleChooseTime('1')}>1 сентября</li>
                            </HeaderDateList>
                        </HeaderUserPopup>
                    </HeaderUserContainer>
                </HeaderRight>
            </HeaderInner>
        </HeaderContainer>
    )
}

export default Header;
