import { Span16, Span20 } from 'src/components/common/typography';
import styled from 'styled-components';
import { Layout } from '../../common/layout'


export const HeaderContainer = styled.header`
    width: 100%;
    height: 76px;
    box-shadow: rgb(0 0 0 / 18%) 0px 1px 12px;
    z-index: 1000;
    display: flex;
    align-items: center;
`;

export const HeaderInner = styled(Layout)`
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

export const HeaderLeft = styled.a`
    display: flex;
    cursor: pointer;
`;

export const HeaderRight = styled.div`
    display: flex;
    align-items: center;

    > img {
        height: 30px;
        width: 30px;
        margin-right: 30px;
    }
`;

export const HeaderUserContainer = styled.div`
    margin-left: 32px;
    display: flex;
    align-items: center;
    cursor: pointer;
    position: relative;
`;

export const HeaderUserPopup = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    display: ${({ visible }) => visible ? 'flex' : 'none'};
    width: 451px;
    min-height: 378px;
    padding: 48px 55px;
    flex-direction: column;
    background: #FFFFFF;
    border: 1px solid rgba(0, 0, 0, .5);
    z-index: 10000;
    cursor: default;
`;

export const HeaderUserPopupTitle = styled(Span20)`
    font-weight: 400;
    margin-bottom: 47px;
`;

export const HeaderUserPopupButton = styled(Span16)`
    margin-bottom: 25px;
    font-weight: 400;
    cursor: pointer;
`;

export const HeaderUserName = styled(Span16)`
    font-weight: 700;
    color: black;
    margin-right: 8px;
    max-width: 250px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
`;

export const HeaderUserAvatar = styled.img`
    width: 48px;
    height: 48px;
`;

export const HeaderDateList = styled.ul`
    list-style: none;
    display: ${({ visible }) => visible ? 'block' : 'none'};

    > li {
        cursor: pointer;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 24px;
        letter-spacing: -0.035em;
        color: #1A1A1A;
    }
`;
