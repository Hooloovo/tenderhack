import React, { useRef } from 'react';
import Slider from 'react-slick';
import Category from '../category';
import { SliderArrow } from '../productSlider/styled';
import { SliderContainer } from './styled';
import { v4 } from 'uuid';
import { multiplyItems } from 'utils';


const CategorySlider = ({ categories }) => {
    const ref = useRef();
    const settings = {
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                }
            },
          ]
    };
    return (
        <SliderContainer>
            <SliderArrow isLeft onClick={() => ref.current.slickPrev()} >{'<'}</SliderArrow>
            <SliderArrow onClick={() => ref.current.slickNext()} >{'>'}</SliderArrow>
            <Slider {...settings} ref={ref}>
                {multiplyItems(categories).map((category, i) => 
                    <Category
                        key={v4}
                        id={category.info.id}
                        name={category.info.name}
                        subcategories={category.subcategories}
                    />
                )}
            </Slider>
        </SliderContainer>
    )
}

export default CategorySlider;
