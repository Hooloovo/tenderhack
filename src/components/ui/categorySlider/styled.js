import styled from 'styled-components';


export const SliderContainer = styled.div`
    padding-top: 50px;
    position: relative;

    .slick-list {
        padding: 50px 0;
    }
`;
