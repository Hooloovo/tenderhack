import styled from 'styled-components';
import { Span14, Span28 } from 'src/components/common/typography';


export const SearchBarContainer = styled.div`
    width: 100%;
    height: 37px;
    display: flex;
    justify-content: space-between;
    margin: 32px 0;
`;

export const SearchBarLeft = styled.div`
    display: flex;
    flex-direction: column;
`;

export const SearchContainer = styled.div`
    width: calc(100% - 290px);
    position: relative;
`;

export const SearchInput = styled.input`
    width: 100%;
    height: 100%;
    padding: 10px 37px 10px 14px;
    background-color: #cce2ff;
    border: 1px solid #e7eef7;
    color: #1a1a1a;
    border-radius: 0;
    font-size: 14px;
    line-height: 17px;
    font-weight: 400;
`;

export const SearchButton = styled.img`
    position: absolute;
    right: 0;
    top: 0;
    width: 37px;
    height: 37px;
`;

export const SearchHr = styled.div`
    content: ' ';
    margin-top: 48px;
    width: 100%;
    border-bottom: 2px solid rgb(231, 238, 247);
`;

export const AllCategories = styled.span`
    font-size: 16px;
    font-weight: 600;
    line-height: 20px;
    color: rgb(38, 75, 130);
    margin-top: 16px;
`;
