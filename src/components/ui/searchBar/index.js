import React from 'react';
import { Layout } from 'src/components/common/layout';
import { Span14, Span28 } from 'src/components/common/typography';
import { AllCategories, SearchBarContainer, SearchBarLeft, SearchButton, SearchContainer, SearchHr, SearchInput } from './styled';


const SearchBar = () => {
    return (
        <>
            <Layout>
                <SearchBarContainer>
                    <SearchBarLeft>
                        <Span28>Реестр товаров</Span28>
                        <AllCategories>Все категории</AllCategories>
                    </SearchBarLeft>
                    <SearchContainer>
                        <SearchInput placeholder="Введите название категории или товара" />
                        <SearchButton />
                    </SearchContainer>
                </SearchBarContainer>
            </Layout>
            <SearchHr />
        </>
    )
}


export default SearchBar;
