import { Layout } from 'src/components/common/layout';
import { Span28 } from 'src/components/common/typography';
import styled from 'styled-components';


export const TableWrapper = styled(Layout)`
    display: flex;
    flex-direction: column;
    margin-bottom: 83px;
`;

export const TableTitle = styled(Span28)`
    margin-bottom: 30px;
`;

export const TableContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(8, 140px);
    grid-column-gap: 25px;
    grid-row-gap: 48px;
    justify-items: center;

    > input {
        cursor: pointer;
    }
`;

export const TableColumnName = styled.span`
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 16px;
    color: #333333;
`;
