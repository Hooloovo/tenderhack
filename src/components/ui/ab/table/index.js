import React, { useState } from 'react';
import { TableColumnName, TableContainer, TableTitle, TableWrapper } from './styled';
import { v4 } from 'uuid';


const Table = () => {
    const [tests, setTests] = useState([
        [false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false],
    ])

    const handleChange = (i, j) => {
        const newTests = tests.concat([]);

        newTests[i][j] = !newTests[i][j];

        setTests(newTests);
    }

    return (
        <TableWrapper>
            <TableTitle>Тестирования в процессе</TableTitle>
            <TableContainer>
                <div />
                <TableColumnName>Баннер “Новый год”</TableColumnName>
                <TableColumnName>Баннер “День защитника отчества”</TableColumnName>
                <TableColumnName>Блок “С этим товаром покупают”</TableColumnName>
                <TableColumnName>Блок “Вы уже заказывали”</TableColumnName>
                <TableColumnName>Письмо с повтором покупки</TableColumnName>
                <TableColumnName>Блок “Вам может быть интересно” при регистрации</TableColumnName>
                <TableColumnName>Блок “Вам может быть интересно” при повторном входе</TableColumnName>


                {tests.map((elem, i) => 
                    <>
                        <TableColumnName>10 % пользователей</TableColumnName>
                        {elem.map((test, j) =>
                            <input key={v4()} type="checkbox" checked={test} onChange={() => handleChange(i, j)} />
                        )}
                    </>
                )}
            </TableContainer>
        </TableWrapper>
    )
}


export default Table;
