import React from 'react';
import { Layout } from 'src/components/common/layout';
import { HeadingContainer, HeadingTitle } from './styled';


const Heading = () => {
    return (
        <HeadingContainer>
            <Layout>
                <HeadingTitle>Тесты</HeadingTitle>
            </Layout>
        </HeadingContainer>
    )
}


export default Heading;
