import styled from 'styled-components';


export const HeadingContainer = styled.div`
    padding-top: 32px;
    width: 100%;
    border-bottom: 3px solid #E8EEF6;
    padding-bottom: 17px;
    margin-bottom: 48px;
`;

export const HeadingTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 37px;
    color: #333333;
`;
