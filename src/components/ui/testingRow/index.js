import React from 'react';
import TestingCard from '../testingCard';
import { RowContainer, RowTitle, RowWrapper } from './styled';


const TestingRow = ({ submitable }) => {
    return (
        <RowContainer>
            <RowTitle>{submitable ? 'Статистика по тестам' : 'Завершенные тестирования'}</RowTitle>
            <RowWrapper>
                <TestingCard
                    onSubmit={submitable ? () => ({}) : undefined}
                    reason="1"
                    description="1234"
                    startDate="15 ноября 2020"
                    endDate="15 декабря 2020"
                    groups={[
                        {
                            middleCheck: 290,
                            checkIncreasePercent: -1,
                        },
                        {
                            middleCheck: 340,
                            checkIncreasePercent: 10,
                        },
                    ]}
                />
                <TestingCard
                    onSubmit={submitable ? () => ({}) : undefined}
                    reason="1"
                    description="1234"
                    startDate="15 ноября 2020"
                    endDate="15 декабря 2020"
                    groups={[
                        {
                            middleCheck: 290,
                            checkIncreasePercent: -1,
                        },
                        {
                            middleCheck: 340,
                            checkIncreasePercent: 10,
                        },
                    ]}
                />
                <TestingCard
                    onSubmit={submitable ? () => ({}) : undefined}
                    reason="1"
                    description="1234"
                    startDate="15 ноября 2020"
                    endDate="15 декабря 2020"
                    groups={[
                        {
                            middleCheck: 290,
                            checkIncreasePercent: -1,
                        },
                        {
                            middleCheck: 340,
                            checkIncreasePercent: 10,
                        },
                    ]}
                />
                <TestingCard
                    onSubmit={submitable ? () => ({}) : undefined}
                    reason="1"
                    description="1234"
                    startDate="15 ноября 2020"
                    endDate="15 декабря 2020"
                    groups={[
                        {
                            middleCheck: 290,
                            checkIncreasePercent: -1,
                        },
                        {
                            middleCheck: 340,
                            checkIncreasePercent: 10,
                        },
                    ]}
                />
            </RowWrapper>
        </RowContainer>
    )
}


export default TestingRow;
