import { Layout } from 'src/components/common/layout';
import { Span28 } from 'src/components/common/typography';
import styled from 'styled-components';


export const RowContainer = styled(Layout)`
    width: 100%;
    display: flex;
    flex-direction: column;
    margin-top: 83px !important;
`;

export const RowTitle = styled(Span28)`
    margin-bottom: 36px;
`

export const RowWrapper = styled.div`
    display: flex;
    padding: 40px;
    background: #E8EEF6;
    border-radius: 4px;
    overflow-x: scroll;
`;
