import { Span28, Span16 } from 'src/components/common/typography';
import styled from 'styled-components';


export const ProductContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
`;

export const ProductTitle = styled(Span28)`
    margin-bottom: 5px;
`;

export const ProductSubtitle = styled(Span16)`
    color: rgba(0,0,0,.6);
`;

export const ProductContentTable = styled.div`
    width: 100%;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;
`;

export const ProductImage = styled.img`
    width: 100%;
    object-fit: scale-down;
`;

export const ProductStats = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

export const ProductStatsItem = styled.div`
    display: flex;
    flex-direction: column;
`;

export const ProductStatsName = styled.span`
    color: #8b8b8b;
    font-size: 15px;
    line-height: 19px;
    font-weight: 700;
`;

export const ProductStatsValue = styled.span`
    font-size: 15px;
    line-height: 19px;
    font-weight: 400;
    color: black;
`;

export const ProductButton = styled.button`
    box-shadow: inset 0 0 0 0 rgb(34 36 38 / 15%);
    background-color: #7f8792;
    color: #fff;
    text-shadow: none;
    cursor: pointer;
    font-weight: 700;
    line-height: 1em;
`;
