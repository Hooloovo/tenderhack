import React from 'react';
import { Span28, Span20 } from 'src/components/common/typography';
import { ProductButton, ProductContainer, ProductContentTable, ProductImage, ProductStats, ProductStatsItem, ProductStatsName, ProductStatsValue, ProductSubtitle, ProductTitle } from './styled';


const Product = ({ title, model, maker, image, id, country, price, minPrice, maxPrice }) => {
    return (
        <ProductContainer>
            <ProductTitle>{title}</ProductTitle>
            <ProductSubtitle>Модель: {model}</ProductSubtitle>
            <ProductSubtitle>Производитель: {maker}</ProductSubtitle>
            <ProductContentTable>
                <ProductImage src={image} />
                <ProductStats>
                    <ProductStatsItem>
                        <ProductStatsName>ID СТЕ</ProductStatsName>
                        <ProductStatsValue>{id}</ProductStatsValue>
                    </ProductStatsItem>
                    <ProductStatsItem>
                        <ProductStatsName>Страна происхождения</ProductStatsName>
                        <ProductStatsValue>{country}</ProductStatsValue>
                    </ProductStatsItem>
                    <ProductStatsItem>
                        <Span28>{price} ₽</Span28>
                        <ProductStatsValue>штука</ProductStatsValue>
                    </ProductStatsItem>
                    <ProductStatsItem>
                        <Span20>от {minPrice} ₽ до {maxPrice} ₽</Span20>
                    </ProductStatsItem>
                </ProductStats>
                <ProductStats>
                    <ProductStatsItem>
                        <ProductStatsName>Контрактов</ProductStatsName>
                        <ProductStatsValue>Контрактов по товару нет</ProductStatsValue>
                    </ProductStatsItem>
                </ProductStats>
            </ProductContentTable>
        </ProductContainer>
    )
}


export default Product;
