import Link from 'next/link';
import React from 'react';
import { Button16 } from '../../common/typography';
import { v4 } from 'uuid'
import { Button, CategoryContainer, CategoryContentContainer, CategoryImage, CategoryItem, CategoryTitle } from './styled';


const Category = ({ id, name, subcategories }) => {
    return (
        <CategoryContainer>
            <CategoryImage src="https://zakupki.mos.ru/cms/Media/ServicesIcon/%D0%91%D1%8B%D1%82%D0%BE%D0%B2%D0%B0%D1%8F%20%D1%82%D0%B5%D1%85%D0%BD%D0%B8%D0%BA%D0%B0.svg" />
            <CategoryContentContainer>
                <Link href={`/category/${id}`}>
                    <CategoryTitle>{name}</CategoryTitle>
                </Link>
                {subcategories.slice(0, 6).map((subcategory, i) =>
                    <Link key={v4()} href={`/category/${subcategory.id}`}>
                        <CategoryItem>{subcategory.name}</CategoryItem>
                    </Link>
                )}
            </CategoryContentContainer>
            <Button>
                <Link href={`/category/${id}`}>
                    <Button16>Смотреть все</Button16>
                </Link>
            </Button>
        </CategoryContainer>
    )
}

export default Category;
