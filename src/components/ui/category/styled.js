import styled from 'styled-components';
import { Span16, Span20 } from '../../common/typography';


export const CategoryContainer = styled.div`
    width: 295px;
    height: 400px;
    padding: 80px 30px 30px 30px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    position: relative;
    background-color: #E7EEF7;
`;

export const CategoryImage = styled.img`
    height: 100px;
    align-self: center;
    position: absolute;
    top: -50px;
`;

export const CategoryContentContainer = styled.div`
    display: flex;
    flex-direction: column;
`;

export const CategoryTitle = styled.a`
    font-size: 20px;
    line-height: 24px;
    color: #1a1a1a;
    font-weight: 700;
    margin-bottom: 10px;
    text-decoration: none;
    cursor: pointer;
`;

export const CategoryItem = styled.a`
    font-size: 16px;
    line-height: 18px;
    margin: 7px 0;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    color: #264b82;
    font-weight: 400;
    cursor: pointer;
`;

export const Button = styled.button`
    width: fit-content;
    padding: 8px 14px;
    border: 1px solid rgb(212, 219, 230);
`;

export const CategoryButton = styled(Button)`
    align-self: flex-end;
`;
