import styled from 'styled-components';
import { Layout } from '../../common/layout';
import { Span20, Span28 } from '../../common/typography/index'


export const BannerContainer = styled.div`
    width: 100%;
    height: 245px;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

export const BannerInner = styled(Layout)`
    display: flex;
    justify-content: space-between;
`;

export const BannerContent = styled.div`
    display: flex;
    flex-direction: column;
    z-index: 100;
`;

export const BannerTitle = styled(Span28)`
    color: #fff;
    margin-bottom: 16px;
`;

export const BannerSubtitle = styled(Span20)`
    color: #fff;
    font-weight: 400;
`;

export const BannerButton = styled.a`
    padding: 14px 32px;
    background: #CA2A26;
    border: none;
    font-size: 20px;
    line-height: 27px;
    color: #FFFFFF;
    z-index: 100;
    display: flex;
    align-items: center;
`;

export const BannerImage = styled.img`
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
    z-index: -1;
`;
