import React from 'react';
import Link from 'next/link';
import { Layout } from '../../common/layout';
import { BannerButton, BannerInner, BannerContainer, BannerContent, BannerImage, BannerSubtitle, BannerTitle } from './styled';


const Banner = ({ title, subtitle, image, buttonText, disableButton }) => {
    return (
        <BannerContainer>
            <BannerImage src={image} />
            <BannerInner>
                <BannerContent>
                    <BannerTitle>{title}</BannerTitle>
                    <BannerSubtitle>{subtitle}</BannerSubtitle>
                </BannerContent>
                {!disableButton && (
                    <Link href="/special">
                        <BannerButton>{buttonText}</BannerButton>
                    </Link>
                )}
            </BannerInner>
        </BannerContainer>
    )
}

export default Banner;
