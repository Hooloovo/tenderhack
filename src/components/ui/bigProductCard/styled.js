import { Span16, Span20 } from 'src/components/common/typography';
import styled from 'styled-components';


export const BigProductCardContainer = styled.a`
    cursor: pointer;
    width: 100%;
    height: 100%;
    padding: 30px;
    display: flex;
    flex-direction: column;
    border: 1px solid rgb(212, 219, 230);
`;

export const BigProductCardImage = styled.img`
    width: 100%;
    object-fit: scale-down;
    margin-bottom: 24px;
`;

export const BigProductCardTitle = styled(Span16)`
    margin-bottom: 32px;
`;

export const BigProductCardPrice = styled(Span20)`
    color: #264b82;
`;
