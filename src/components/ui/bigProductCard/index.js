import React from 'react';
import { BigProductCardContainer, BigProductCardImage, BigProductCardPrice, BigProductCardTitle } from './styled';


const BigProductCard = ({ image, title, price }) => {
    return (
        <BigProductCardContainer>
            <BigProductCardImage src={image} />
            <BigProductCardTitle>{title}</BigProductCardTitle>
            <BigProductCardPrice>{price}</BigProductCardPrice>
        </BigProductCardContainer>
    )
}


export default BigProductCard;
