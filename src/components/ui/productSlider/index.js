import React, { useRef } from 'react';
import { ProductCard } from '../productCard';
import { SliderArrow, SliderContainer, SliderStyled, SliderTitle } from './styled';
import { v4 } from 'uuid';
import { multiplyItems } from 'utils';


export const ProductSlider = ({ title, cards }) => {
    const ref = useRef();
    const settings = {
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1600,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
              }
            },
            {
                breakpoint: 1400,
                settings: {
                  slidesToShow: 4,
                  slidesToScroll: 1,
                }
              },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            }
          ]
        // nextArrow: <SliderArrow isLeft ><div /></SliderArrow>,
        // prevArrow: <SliderArrow ><div /></SliderArrow>
    };
    return (
        <SliderContainer>
            <SliderTitle>{title}</SliderTitle>
            <SliderArrow isLeft onClick={() => ref.current.slickPrev()}>{'<'}</SliderArrow>
            <SliderStyled {...settings} ref={ref}>
                {multiplyItems(cards).map((card, i) =>
                  <ProductCard
                    key={v4()}
                    id={card.id}
                    image={card.image}
                    price={card.avgPrice}
                    count={card.viewCount}
                    name={card.name}
                  />
                )}
            </SliderStyled>
            <SliderArrow onClick={() => ref.current.slickNext()} >{'>'}</SliderArrow>
        </SliderContainer>
    )
}
