import styled from 'styled-components';
import Slider from 'react-slick'
import { Span28 } from '../../common/typography';


export const SliderContainer = styled.div`
    width: 100%;
    height: 460px;
    padding: 50px 0;
    display: flex;
    flex-direction: column;
    position: relative;
`;

export const SliderTitle = styled(Span28)`
    margin-bottom: 16px;
`;

export const SliderStyled = styled(Slider)``;

export const SliderArrow = styled.div`
    width: 50px;
    height: 50px;
    border-radius: 25px;
    cursor: pointer;
    border: 1px solid rgba(0, 0, 0, 0.2);
    display: flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    top: calc(50%);
    ${props => props.isLeft ? 'left: -50px' : 'right: -50px'};

    &:hover {
        background-color: rgb(38, 75, 130);
        color: white;
    }

    > div {
        content: "${props => props.isLeft ? "\f104" : "\f105"}";
    }
`;
