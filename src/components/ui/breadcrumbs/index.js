import React from 'react';
import Link from 'next/link';
import { Breadcrumb, BreadcrumbContainer } from './styled';
import { v4 } from 'uuid'


export const Breadcrumbs = ({ path }) => {
    return (
        <BreadcrumbContainer>
            <Link href={`/`}><Breadcrumb>Товары</Breadcrumb></Link>
            {path.map((elem, key) => key === 0 ? (
                <Link href={`/category/${elem.id}`}>
                    <Breadcrumb>{elem.name}</Breadcrumb>
                </Link>
            ) : (
                <Link href={`/category/${elem.id}`} key={v4()}><Breadcrumb>{elem.name}</Breadcrumb></Link>
            )
            )}
        </BreadcrumbContainer>
    )
}
