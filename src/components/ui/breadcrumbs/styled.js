import styled from 'styled-components';


export const BreadcrumbContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    margin-top: 16px;
    margin-bottom: 32px;
`;

export const Breadcrumb = styled.a`
    color: #264b82;
    font-size: 14px;
    line-height: 20px;
    text-decoration: none;
    cursor: pointer;

    &:not(:last-child):after {
        content: '>';
        margin: 10px;
    }
`;
