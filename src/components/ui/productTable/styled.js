import { Layout } from 'src/components/common/layout';
import styled from 'styled-components';


export const ProductTableContainer = styled(Layout)`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(290px, 1fr));
    grid-column-gap: 28px;
    grid-row-gap: 28px;
`;
