import React from 'react';
import Link from 'next/link';
import BigProductCard from '../bigProductCard';
import { ProductTableContainer } from './styled';
import { v4 } from 'uuid'


const ProductTable = ({ cards }) => {
    return (
        <ProductTableContainer>
            {cards.map((card, i) => 
                <Link href={`/${card.id}`} key={v4()}>
                    <a>
                        <BigProductCard
                            image={card.image}
                            title={card.name}
                            price={card.avgPrice}
                        />
                    </a>
                </Link>
            )}
        </ProductTableContainer>
    )
}


export default ProductTable;
