import styled from 'styled-components';


export const Layout = styled.div`
    @media only screen and (min-width: 1400px) {
        width: 1327px;
    }

    @media only screen and (min-width: 992px) and (max-width: 1399.98px) {
        width: 933px;
    }

    @media only screen and (min-width: 768px) and (max-width: 991.98px) {
        width: 723px;
    }

    margin: auto;
`;
