import styled from 'styled-components';


export const Span28 = styled.span`
    font-size: 28px;
    line-height: 36px;
    color: #1a1a1a;
    font-weight: 700;
`;

export const Span20 = styled.span`
    font-size: 20px;
    line-height: 24px;  
    color: #1a1a1a;
    font-weight: 700;
`;

export const Span14 = styled.span`
    font-size: 14px;
    line-height: 20px;
    color: #1a1a1a;
    font-weight: 400;
`;

export const Span16 = styled.span`
    font-size: 16px;
    line-height: 18px;
`;

export const Button16 = styled.a`
    font-size: 16px;
    line-height: 18px;  
    color: #264b82;
    font-weight: 600;
    text-decoration: none;
`;
