import axios from 'axios';

const server = 'https://tenderhack-api.ulanzetz.com/api';


export const getRecommended = (customerId) => 
    axios.get(`${server}/recommended/goods/${customerId}`)
    .then(({ data }) => data);

export const getCategories = (customerId) => 
    axios.get(`${server}/recommended/categories/${customerId}`)
    .then(({ data }) => data);

export const getProduct = (id) => 
    axios.get(`https://old.zakupki.mos.ru/api/Cssp/Sku/GetEntity?id=${id}`)
    .then(({ data }) => ({
        title: data.name,
        model: data.modelName,
        maker: data.vendorName,
        image: data.images.length > 0 && `https://zakupki.mos.ru/newapi/api/Core/Thumbnail/${data.images[0].fileStorage.id}/300/300`,
        id: data.id,
        country: data.oksmName,
        price: Math.round(data.referencePrice),
        minPrice: data.minPrice,
        maxPrice: data.maxPrice
    }))

export const getProductBreadcrumbs = (id) => axios.get(`${server}/info/good/${id}`)
    .then(({ data }) => [data.category, data.subcategory]);

export const getCategoryFamily = (categoryId) => axios.get(`${server}/info/category/${categoryId}`)
    .then(({ data }) => data.parent === undefined ? [data.info] : [data.info, data.parent])

export const getRandomCustomer = () => axios.get(`${server}/info/randomCustomer`)
    .then(({ data }) => data);

export const getRecentProducts = (customerId) => axios.get(`${server}/info/goods/recent/${customerId}`)
    .then(({ data }) => data);

export const getRecommendedSubcategory = (customerId, subcategoryId) => axios.get(`${server}/recommended/goods/${customerId}?subcategoryId=${subcategoryId}`)
    .then(({ data }) => data);

export const getSimular = (customerId, cartGoodId) => axios.get(`${server}/recommended/goods/${customerId}?cartGoodId=${cartGoodId}`)
    .then(({ data }) => data);

export const getNewYear = () => axios.get(`${server}/recommended/collection/newyear`)
    .then(({ data }) => data)

export const getPopular = () => axios.get(`${server}/recommended/collection/popular`)
    .then(({ data }) => data);

export const getRecommendedCategory = (customerId, categoryId) => axios.get(`${server}/recommended/goods/${customerId}?categoryId=${categoryId}`)
    .then(({ data }) => data)
